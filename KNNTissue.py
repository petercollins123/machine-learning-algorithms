#K Nearest Neighbor(KNN) Instance based learning
#Prediction(Extrapolation)

import time

start_time = time.time()

objects = [(7, 7, "Bad"), (7, 4, "Bad"),(3, 4, "Good"), (1, 4, "Good")]

print("\nExisting datapoints\n")
for x in objects:
    print(x)

print (f"\nNumber of datapoints {len(objects)}.\n")


#######################
# ALGORITHM STARTS HERE
#######################
while(True):

#step one ::determine parameter k->size of datapoints within a cluster
    print("**"*30)
    print("**"*30)
    print(f"\n NB::value of k should be an odd number > 1 and less than {len(objects)}")

    k = int(input(print("\n\nChoose value of k(group size)\n -> ")))

    if( k > 1 and k < len(objects) and k%2 != 0):
        query_instance = []
        for i in range(0,2):            
            query_a = int(input(print(f"\nEnter query instance X{i+1}\n -> ")))
            query_instance.append(query_a)

        #step two ::calculate distance between query instance and all the training samples        
        sorted_objects = sorted(objects, key = lambda x: (query_instance[0] - x[0])**2 + ((query_instance[1] - x[1])**2))
        print("\nSorting...\n\n")
        time.sleep(3)
        #step three ::sort the distances in ascending order        
        print(f"Sorted objects : {objects}")

        #step four ::take the first k tuples
        decision =[]                                #empty list to store k values with min distance from query instance            

        print(f"\nFirst {k} datapoints  :")
        
        for x in range(k):
            print(sorted_objects[x])
            decision.append(sorted_objects[x])

        decision = [x[2] for x in decision]   # get the third value in every decision tuple
        print(f"\nTheir corresponding decisions :\n{decision}") 
                
        choice_a = []                         # empty lists to store votes 
        choice_b = []

        for i in range(k):
            if(decision[i] == "Good"):
                choice_a.append(decision[i])
            else:
                choice_b.append(decision[i])
        
        print("\nVoting...\n")
        time.sleep(3)
        print(f"\nGroup one : {choice_a}\nGroup two : {choice_b}")  #voting and results
        #STEP FIVE 
        lenchoice_a = len(choice_a)
        lenchoice_b = len(choice_b)

        if(lenchoice_a > lenchoice_b):
            print(f"\nPopular decision for {query_instance} is...\n\n")
            time.sleep(2)
            print(f"{choice_a[0]} \n\n")
        else:
            print(f"\nPopular decision for {query_instance} is ...")
            time.sleep(2)
            print(f"{choice_b[0]}\n\n")

        choice =  int(input("\n\nChoose action\n1. Continue testing \n2. Exit"))
        if(choice == 1):
            for x in objects:
                print(x)
            continue
        elif(choice == 2):
            break
        else:
            print("\nInvalid input")
                     
        
    else:

        print("\nERROR!!!!!\nInvalid input")
        choice = int(input(print("\n Choose action\n1. Retry \n2. Exit \n::")))
        if(choice == 1):
            for x in objects:
                print(x)
            continue
        elif(choice == 2):
            break
        else:
            print("\nInvalid input")
            continue       

stop_time = time.time()
time_taken = abs(round((start_time - stop_time), 3))
print(f"\nTime taken : {time_taken} s")