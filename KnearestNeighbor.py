# K Nearest Neighbor(KNN) Instance based learning
# Prediction(Extrapolation)
from functools import  reduce
import time

start_time = time.time()

objects = [(1, 5), (2, 17),(3, 9), (4, 1), (5, 19), (7,10)]

print("\nExisting datapoints")
for x in objects:
    print(x)

print (f"\nNumber of datapoints {len(objects)}.")
print("\n*****************\n")

#######################
# ALGORITHM STARTS HERE
#######################

while(True):

#step one ::determine parameter k->size of datapoints within a cluster

    k = int(input("\nSTEP ONE: GETTING VALUE OF K\n\nChoose value of k(group size): "))

    if( k > 1 and k < len(objects)):
            
        query_instance = int(input("\nEnter query_instance(x-value): "))
        #step two ::calculate distance between query instance and all the training samples
        print("\nSTEP TWO :: ...calculation of query_distance from other datapoints\n")
        objects = sorted(objects, key = lambda x: abs(x[0] - query_instance), reverse = False)


        #step three ::sort the distances in ascending order
        print("\nSTEP THREE :: Datapoints in ascending order in terms of distance from query instance\n")
        print(objects)

        #step four ::take the first k(3) tuples
        print(f"\nSTEP FOUR :: chosen values based on k: {objects[:k]}\n")

        correspondingValue = reduce(lambda x,y: x + y[1],objects[:k], 0)/k

        print(f"\nSTEP FIVE :: RESULT")
        print(f" \nCorresponding y value of {query_instance} is {correspondingValue}")
        choice =  int(input("\n Choose action\n1. Continue testing \n2. Exit"))
        if(choice == 1):
            for x in objects:
                print()
            continue
        elif(choice == 2):
            break
        else:
            print("\nInvalid input")
        
    else:
        print("\nERROR!!!!!\nInvalid input")
        choice = int(input(print("\n Choose action\n1. Retry \n2. Exit")))
        if(choice == 1):
            for x in objects:
                print()
            continue
        elif(choice == 2):
            break
        else:
            print("\nInvalid input")
            break

stop_time = time.time()

time_taken = abs(round((start_time - stop_time), 3))

print(f"\nTime taken : {time_taken} s")