#BAYESIAN THEOREM
#P(A/B) = (P(B/A)*P(A))/P(B)
import time
#car data from survey
secondHand  = [25, 30, 40]
newVehicle  = [15, 65, 75]
cars = [secondHand, newVehicle]

#levels of income
lowIncome = [secondHand[0], newVehicle[0]]
mediumIncome = [secondHand[1], newVehicle[1]]
highIncome = [secondHand[2], newVehicle[2]]

incomeLevel = [lowIncome, mediumIncome, highIncome]
#conditional probability by row
print("\nCONDITIONAL PROBABILITY")
conditionalCarsByRow = []
for items in cars:
    row = []
    for value in items:
        row.append(round(((value/sum(items))*100), 1))
    conditionalCarsByRow.append(row)
print(f"\n\n by row\n")
print("\t\tLow Medium  High   Total")
print(f"\nSecond hand : {conditionalCarsByRow[0]} {sum(conditionalCarsByRow[0])} ")
print(f"\nNew Vehicle : {conditionalCarsByRow[1]} {sum(conditionalCarsByRow[1])}")

#conditional probability by row
conditionalCarsByColumn = []
for items in incomeLevel:
    column = []
    for value in items:
        column.append(round(((value/sum(items))*100),1))
    conditionalCarsByColumn.append(column)
    
print(f"\n\n by column\n")
print(conditionalCarsByColumn)
print("\n\t\t Low Medium  High  ")
print(f"SecondHand  : {conditionalCarsByColumn[0][0]} {conditionalCarsByColumn[1][0]} {conditionalCarsByColumn[2][0]}")
print(f"New Vehicle : {conditionalCarsByColumn[0][1]} {conditionalCarsByColumn[1][1]} {conditionalCarsByColumn[2][1]}")
print(f"Total       : {sum(conditionalCarsByColumn[0])} {sum(conditionalCarsByColumn[1])}  {sum(conditionalCarsByColumn[2])}")

#marginal probability
# print(lowIncome[0]/sum([sum(cars[0]),sum(cars[1])]))
marginal = []
# conditionalCarsByRow = []
for items in cars:
    row = []
    for value in items:
        row.append(round(((value/sum([sum(cars[0]),sum(cars[1])]))*100), 1))
    marginal.append(row)

print("\t\tLow Medium  High   Total")
print(f"\nSecond hand : {marginal[0]} {sum(marginal[0])} ")
print(f"New Vehicle : {marginal[1]} {sum(marginal[1])}")
totalMarginal = []
for value in range(len(marginal[0])):    
    totalMarginal.append((marginal[0][value] + marginal[1][value]))

print(f"Total       : {totalMarginal} {sum(totalMarginal)}")

#joint probability
jointProbability = []
for car in cars:
    print(car)
    row = []
    counter = 0
    for item in range(len(car)):
        print(conditionalCarsByRow[counter][item])
        print(marginal[counter][item])
        # input()
        row.append(conditionalCarsByRow[counter][item] * marginal[counter][item])
    counter+=1
    jointProbability.append(row)

for row in jointProbability:
    print(row)
