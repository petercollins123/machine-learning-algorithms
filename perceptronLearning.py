#PERCEPTRON LEARNING ALGORITHM
import time
from perceptronElectionData import trainingSet as importedDataSet

print("\nPERCEPTRON TO PERFORM A BINARY NAND FUNCTION ON GIVEN INPUTS")
threshold = 0.5
bias = 0
learningRate = 0.1

dataset = importedDataSet
datasetLength = len(dataset)                              

# weights = [0, 0, 0, 0]   #we can also use a for loop to initialize the weights
weights  = []
for weight in range(len(dataset[0][0])):
    weights.append(0)

print(weights)

if(len(dataset[0][0]) != len(weights)):
    print(f"\nERROR!!Incorrect weight input mapping")
    time.sleep(1.5)

finalWeights = []
perSensor = []
#function to compare list items
def same(items):    
    return all(x == items[0] for x in items)
     
def batch(trainingSet, targetValue, initialWeights):    
    perSensorSum = 0
    #per sensor
    for position in range(len(trainingSet)):        
        sensor = round((trainingSet[position]*initialWeights[position]), 4)
        perSensorSum += sensor
        perSensor.append(sensor)
        
    # print(f"\nPer sensors : {perSensor}")
    # print(f"\nSum == {round(perSensorSum, 3)}")
    
    network = 1 if (perSensorSum>threshold) else 0

    #error    
    error = targetValue - network
    # print(f"\nError {error}")
    #correction
    d = learningRate*error
    #Final weights  correction + old 
    weights = []
    for position in range(len(trainingSet)):
        newWeight = round(((trainingSet[position]*d) + initialWeights[position]), 4)
        weights.append(newWeight)
    finalWeights.append(weights)

    perSensor.clear()

    return weights   

rounds = 1
while(True):
    print("*"*60)
    print("*"*60)
    print(f"\n*************** EPOCH {rounds} ***************************")        
    for i, dataPoint in enumerate(dataset):
        print("-"*30)
        print(f"\nBatch {i+1}")
        print("-"*30)
        weights = batch(
            trainingSet=dataPoint[0],
            targetValue=dataPoint[1],
            initialWeights=weights
        )               
#stopping condition 

        if rounds >= datasetLength:   
            compareWeights = []                 #clear compare after each epoch to aid in same(list) function
            counter = 1
            while(counter <= datasetLength):    #add the last n weights to compare        
                position = len(finalWeights) - counter
                compareWeights.append(finalWeights[position])
                counter+=1

            epochIsConsistent = same(compareWeights)
            if epochIsConsistent:                
                print("\n\t\tNEW WEIGHTS")  
                
                epochCounter = 1                            #displaying all new weights per epoch
                start = 0
                end = datasetLength
                while(epochCounter<=rounds):
                    print(f"\nEpoch {epochCounter}")                     
                    if(epochCounter == rounds):
                        for weight in range(start, (len(finalWeights))):
                            print(finalWeights[weight])
                        break
                    for weight in range(start,end):           
                        print(f"{finalWeights   [weight]}")
                    start = epochCounter * datasetLength
                    end = start + datasetLength
                    epochCounter += 1
                    
                print(f"\nCONVERGENCE :: epoch {rounds}")
                print(f"\nStabilized Weights\n")
                for weight in compareWeights:
                    print(f"\t{weight}")            
                print(f"\nFinal weights\n==> {compareWeights[0]}")
                print("\nWeights have stabilized for all the inputs\n")                
                break
                
        print(f"\nEnd of batch {i+1}")
        print("-"*30)    
        print("*"*30)
    print(f"\nEnd of epoch {rounds}")
    if(rounds >= datasetLength and epochIsConsistent):                
            print("\n*****************  End of perceptron learning  ***********")
            print("*"*58)
            break          
    rounds += 1
    # time.sleep(1)