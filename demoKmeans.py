# K Means Clustering

import time, sys
from functools import reduce

start_time = time.time()

print("\nDISCLAIMER:::\nfor this K means cluster implementation, the Value of K is fixed and arbitrary")
a = 2
print(f"\nThe number of clusters(groups) thereof are also limited based on the size of k = {a} in this case")

objects = [(5, 4), (1, 1), (2, 1), (4, 3), (4, 9), (44, 9), (42, 9)]

print("\nDATA COLLECTION\nExisting datapoints")
for x in objects:
    print(x)
objectSize = len(objects)
print(f"Number of datapoints: {objectSize}.")
print("\n*****************\n")

######################
#ALGORITHM STARTS HERE
#######################


#step one :: Determine parameter k-> number of clusters     
#       LET USER CHOOSE K

k = a
#step two :: Pick the centroids(<randomly> or <first k datapoints(used))>)

c1 = objects[0]
c2 = objects[1]


def distanceFromCentroid(centroid1, centroid2 , x, y):
    from_c1 = (centroid1[0] - x)**2 + (centroid1[1] - y)**2
    from_c2 = (centroid2[0] - x)**2 + (centroid2[1] - y)**2

    from_c1 = round(from_c1, 2)
    from_c2 = round(from_c2, 2)
    return from_c1, from_c2

lengthOne = []
lengthTwo = []

#from step three(Iterative)                                                                 
# ITERATIVE STEP NOT DONE YET

        #sub-step one   :: get coordinates of centroid  
print("\n\n\n\n")  
def repeatedSection(objects, centroid1, centroid2):
    lengthOne = []
    lengthTwo = []
        #sub-step two   :: compute distance between objects and centroids
    
    print(f"\n\n::::STEP 3 part 1  :: Coordinates of the {k} centroids->")
    print(f"\nCentroids\nCentroid1 : {centroid1}\nCentroid2 : {centroid2}")

    distance = []

    for position in objects:
        distance.append(distanceFromCentroid(centroid1, centroid2, position[0], position[1]))

    print(f"\n\n::::STEP 3 part 2  ::Distance of the {len(objects)} datapoints")
    print(" from  c1 and c2(distance_from_c1, distance_from_c2)")    

    print(f"\nDatapoints    :\n {objects}\n")
    print(f"\nFrom centroid :\n {distance}\n")

        #sub-step three :: group based on minimum distance   
    #AUTOMATICALLY GENERATE GROUPS BASED ON VALUE OF 
    
    #get items in pregroups
    pre_groupOne = []
    pre_groupTwo = []
    for item in distance:
        pre_groupOne.append(item[0])
        pre_groupTwo.append(item[1])

    print("\n Groups before sorting")
    print("\nPre group one:", pre_groupOne)
    print("Pre group two:", pre_groupTwo)

    #choose minimums
    
    groupOne = []
    groupTwo = []

    for index in distance: 

        if(index[0] < index[1]):
            groupOne.append(1)
            groupTwo.append(0)

        else:
            groupOne.append(0)
            groupTwo.append(1)
            
            
             #DISPLAY POSITION OF DATAPOINTS BEING DISPLAYED

    print(f"\n\n::::STEP 3 part 3(GROUPING) ::\n\n")
    
    print(f"Group one : {groupOne}\nGroup two : {groupTwo}\n")

    groupOneLength = list(filter(None, groupOne))
    groupTwoLength = list(filter(None, groupTwo))

    groupOneLength = len(groupOneLength)
    groupTwoLength = len(groupTwoLength)  

    #copy data to corresponding group based on value
    
    actualGroupOne = []
    actualGroupTwo = []
    #l.insert(index, obj)
    for i in range(objectSize):
        if(groupOne[i] == 1):
            actualGroupOne.append(objects[i])            
        else:
            actualGroupTwo.append(objects[i])      
    
    

    #get new centroid for created groups by getting average for each group
    #new c1
    new_c1x = round((reduce(lambda x,y: x + y[0], actualGroupOne, 0)/groupOneLength), 2)
    new_c1y = round((reduce(lambda x,y: x + y[1], actualGroupOne, 0)/groupOneLength), 2)

    new_c1 = [new_c1x, new_c1y]

    #new c2
    new_c2x = round((reduce(lambda x,y: x + y[0], actualGroupTwo, 0)/groupOneLength),2)
    new_c2y = round((reduce(lambda x,y: x + y[1], actualGroupTwo, 0)/groupTwoLength),2)

    new_c2 = [new_c2x, new_c2y]   
    
    lengthOne.append(actualGroupOne)
    lengthTwo.append(actualGroupOne)

    print(f"\n****Actual data in clusters****\n")
    print(f"\nCluster 1 : {actualGroupOne}\nCluster 2 : {actualGroupTwo}\n")
    print(f"\nNumber of items")      
    print(f"\n new centroids :\nCentroid 1 : {new_c1}\nCentroid 2 : {new_c2}")
    print(f"\nGroup One : {groupOneLength} \nGroup Two : {groupTwoLength}")  

    return new_c1, new_c2                  
    
print("\n:::::::::::::::::::::START::::::::::::::::::::::") 

initial = repeatedSection(objects, c1, c2)

print(f"\nInitial:{initial}")

#iteration for stability
iteration = []                    
iteration.append(initial)   #list to store new centroids
iteration = [val for sublist in iteration for val in sublist]   #getting the elements in centroid sublist[(a, b)]
compare = [initial]                     #list for storing the 
print(iteration)

for i in range(0, (objectSize - 1)):
    print(f"\n\n:::::::::::::::::::::ITERATION {i+1}::::::::::::::::::::::\n")        
    newC1 = [x[0] for x in iteration]
    newC2 = [x[1] for x in iteration]
    rounds = repeatedSection(objects, newC1, newC2)    
    
    iteration.clear()       #empty list so you can store new centroids in next iteration without growing the list
    iteration.append(rounds)    #add the new centroids iteration list for use in the next round
         #storing the actual elements in the list [(a, b)] 
    
    compare.append(rounds)  #append new centroid to list, the list
    iteration = [val for sublist in compare for val in sublist]   #store accessible list elements(c1, c2) for next iteration 
    
    if(compare[i] == compare[i+1]):
        print(f"\n\n*******Clusters stabilized after {i+1} rounds*****\n\n")
        break
    else:        
        continue

stop_time =  time.time()

time_taken = abs(round((stop_time - start_time), 3))

print(f"\n Time taken : {time_taken} s")
























#groupResults = []

#repeatedSectionResults = repeatedSection()
#groupResults.append(repeatedSectionResults)     #store result of each repeated section 
                                            #in list 'repeated[]'
#print(groupResults)




##POINTS TO NOTE

# ITERATION PHASE NOT COMPLETE
# CURRENTLY USING HARD-CODED VALUE OF K AND HARD-CODING THE CENTROIDS THEREOF


		
		
#reversing lists 
    #groupOne = groupOne[::-1]
    #groupTwo = groupTwo[::-1]
		
		
#groupOne,groupTwo = zip(*sorted(zip(groupOne, groupTwo)))		
		
		

		
		
		
		
		
#distance, objects = zip(*sorted(zip(distance.sort(), objects)))

    #print(f"\n***Sorted***\n{distance}\n{objects}")		
		
		
		
		
		

