# Group 6
# Kohen Self Organizning Maps Algorithm
import sys
import csv
import random
import math
import operator

# Function that  calculates  euclidean distance
# Calculates Best Matching unit
def calcDistance(dataSet1, dataSet2, attrNumber):
    distance = 0
    for x in range(attrNumber):
        distance += pow((dataSet1[x] - dataSet2[x]), 2)
    return distance


# Getting data from file
def readData(filename, pivot, dataSet=[] , testCaseSet=[]):
	with open(filename, 'r') as csvfile:
	    readLines = csv.reader(csvfile, delimiter=' ')
	    data = list(readLines)
		# Range from 0 to datalength - 1
	    for x in range(len(data)):
			# get the data from 0 to 3 
			# depending on the numeric features
	        for y in range(4):
	            data[x][y] = float(data[x][y])
				# Generates random number between 0 - 1 and if less 
				# less than the pivot 
	        if random.random() < pivot:
	            dataSet.append(data[x])
	        else:
	            testCaseSet.append(data[x])

# Randomize weights function
def randomizeWeights(weightsCount,weights):
    for i in range(0, weightsCount):
        randVal = int(random.random()*10) / 10.0
        weights.append(randVal) # you push your element here, as of now I have push i
# Divide the first neuron weights
def getFirstNeuronWeights(firstWeights, weights):
    for i in range(0, 4):
        firstWeights.append(weights[i])
# Divide the second neuron weights
def getSecondNeuronWeights(secondWeights, weights):
      for i in range(4, 8):
        secondWeights.append(weights[i])

# Getting the nearest Neuron
def getLeastRadiusDistance(eucDistances):
    flagIdentifier = []
    if(eucDistances[0] > eucDistances[1]):
        position = 2
        flagIdentifier.append(position)
        flagIdentifier.append(eucDistances[1])
        return flagIdentifier
    else:
        position = 1
        flagIdentifier.append(position)
        flagIdentifier.append(eucDistances[0])
        return flagIdentifier

# Normalization function
def normalization(valList):
	normalizedList = []
	minX = min(valList)
	maxX = max(valList)
	if(maxX > 1): # Do normalization if max is greater than 1
		print ('min and max representaries : ', minX,maxX)
		#Normalized Data
		for item in valList:
			print ('ITEM : ', item)
			normalized = (valList[item]-minX)/(maxX-minX)
			print ('NORM : ', normalized)
			normalizedList.append(normalized)
		print ('NORMALIZED : ', normalizedList)
# Calculates for one input in the set of input
def batch(firstWeights,weights, secondWeights,inputVectors,eucDistances,learningRate,inputCount):
    getFirstNeuronWeights(firstWeights, weights)
    getSecondNeuronWeights(secondWeights, weights)
    print(f"WEIGHTS: {weights}\nSeparated Weights : {firstWeights} | {secondWeights}")
    # 1st input 
    x1 = inputVectors[inputCount]
    print(f"1st input Vector: {inputVectors[inputCount]}\n\n")

    # Calculate First Distance Neuron
    euclDistFirstNeuron = calcDistance(inputVectors[inputCount], firstWeights, 4)
    eucDistances.append(euclDistFirstNeuron)

    # Calculate Second Distance Neuron
    euclDistSecondNeuron = calcDistance(inputVectors[inputCount], secondWeights, 4)
    eucDistances.append(euclDistSecondNeuron)

    print(f"Weights : {firstWeights} | {secondWeights}\nInput: {inputVectors[inputCount]}\nDistances: {eucDistances}\n")
    # STEP 2 COMPUTE WINNER NEURON
    chosenNeuron = getLeastRadiusDistance(eucDistances)
    print(f"Chosen Node: {chosenNeuron}\n\n")
    newFirstWeight = []
    newSecondWeight = []
    #clear list
    weights.clear()
    eucDistances.clear()
    # STEP 3 UPDATE NEURON WEIGHTS
    if(chosenNeuron[0] == 1):
        print(f"Chosen Neuron: [{chosenNeuron[0]}]\n")
        # Adjust the firstWeights ony and update the other weights with old
        for i in range(0, 4):
            newFirstWeight.append(firstWeights[i] + (learningRate * (inputVectors[inputCount][i] - firstWeights[i])))
        print(f"New 1st Weight: {newFirstWeight}\n2nd Weight: {secondWeights}")
        # Update the new weights to the weight list
        for item in newFirstWeight:
            weights.append(round(item,4))
        for items in secondWeights:
            weights.append(round(items,4))
        newFirstWeight.clear()
        firstWeights.clear()
        secondWeights.clear()
        print(f"New Weight [1]: {weights}\n")
    else:
        print(f"Chosen Neuron: [{chosenNeuron[0]}]\n")
        # Adjust the secondWeights
        for i in range(0, 4):
            newSecondWeight.append(secondWeights[i] + (learningRate * (inputVectors[inputCount][i] - secondWeights[i])))
        print(f"New 2nd Weight: {newSecondWeight}\n")
        # Update the new weights to the weight list
        for item in firstWeights:
            weights.append(round(item,4))
        for item in newSecondWeight:
            weights.append(round(item,4))
        firstWeights.clear()
        newSecondWeight.clear()
        secondWeights.clear()
        print(f"New Weight [2]: {weights}\n")

# Main program
def execute():
    print(f"-----------------------INSTRUCTIONS------------------------------------------------------")
    print(f"Solves the following question.")
    print(f"There are 4 input Vectors. Number of neurons to be found 2 and assume learning rate 0.5")
    print(f"-----------------------------------------------------------------------------------------")
    pivot = 1
    weightsCount = 8 # Needed 8 weights because of the # of input vectors
    inputVectors = []
    weights = []
    firstWeights = [] # First Neuron weight
    secondWeights = [] # Second Neuron weight
    eucDistances = []
    testCaseSet = []
    learningRate = 0.5
    # Reads data from file
    readData('som.data',pivot,inputVectors, testCaseSet)
    # readData('pimadiabetes.data.txt',pivot,inputVectors, testCaseSet)
    inputLen = len(inputVectors)
    print(f"GIVEN INPUT VECTORS: [{inputLen}] => {inputVectors} \n\n")
    
    # STEP 1 : INITIALIZE THE WEIGHTS RANDOMLY 
    randomizeWeights(weightsCount,weights)
    #weights = [0.2,0.4,0.6,0.8,0.9,0.7,0.5,0.3]
    print(f"RANDOMIZED WEIGHTS: {weights}\n")
    rounds = 0
    # STEP 4 REPEAT FOR ALL INPUTS
    while(rounds != inputLen):
        print(f"EPOCH : {rounds}\n____________")
        batch(firstWeights,weights, secondWeights,inputVectors,eucDistances,learningRate,rounds)
        rounds+=1

    #append the newWeights to Weight list
    print(f"Final Weight: {weights}\n")

# Run main program
execute()