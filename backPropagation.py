#BACK PROPAGATION   #supervised learning
import math,time
from perceptronElectionData import trainingSet as importedData


toleranceValue = 0.05
learningRate = 0.6
squaredError = []

# dataset = importedData

dataset = [[0.4,-0.7,0.1],
           [0.3,-0.5,0.05],
           [0.6,0.1,0.3],
           [0.2,0.4,0.25],
           [0.1,-0.2,0.12]]

print("\nDataset")          
for data in dataset:
    print(data)

#the input is normalized(-1<=input<=1)

weights = [
    #V_weightVector,
     [
         [0.1,0.4],
         [-0.2,0.2]
      ],
#TW_weightVector
[0.2, -0.5] 
]

def batch(x1,x2,VweightVector, TWweightVector,targetOutput):
    print (TWweightVector)
#transpose of V weight vector
    TVweightVector = [ [VweightVector[0][0],VweightVector[1][0]],
                       [VweightVector[0][1],VweightVector[1][1]] ] 

#input of input layer   == output of output layer
    outputOfInputLayer = [x1, x2]  
    print(f"\nV- weight vector transpose \n {TVweightVector}")
    print(f"\nW- weight vector transpose \n {TWweightVector}")
    print(f"\nTARGET OUTPUT : {targetOutput}")

#input of hidden layer    
    firstRow = round((((TVweightVector[0][0])*x1)+((TVweightVector[0][1])*x2)), 2)
    secondRow = round((((TVweightVector[1][0])*x1)+((TVweightVector[1][1])*x2)), 2)
    hiddenLayerInput = [firstRow,secondRow]
    print(f"\nHidden layer input\n{hiddenLayerInput}")

#output of hidden layer    
    rowOne = round((1/(1+math.exp(-(firstRow)))), 4)
    rowTwo = round((1/(1+math.exp(-(secondRow)))), 4)
    hiddenLayerOutput = [rowOne, rowTwo]
    print(f"\nHidden layer output\n{hiddenLayerOutput}")

#input of output layer
    inputOfOutputLayer = round((TWweightVector[0]*rowOne) + (TWweightVector[1]*rowTwo), 5)
    print(f"\nInput Of output layer\n{inputOfOutputLayer}\n")
#output of output layer
    outputOfOutputLayer = round(1/(1+math.exp(-(inputOfOutputLayer))), 4)    
    print(f"\nOutput Of output layer\n{outputOfOutputLayer}\n")
    print("*"*30)
    print(f"\nOutput Of System : {outputOfOutputLayer}")
    print(f"\nTarget output : {targetOutput}")
    print("*"*30)

#getting the e rror
    error = targetOutput - outputOfOutputLayer    
    errorSquared = round(pow((error),2), 5)
    print(f"\nError Squared: {errorSquared}")
    squaredError.append(errorSquared)

#new Wvector 
#change in Wvector = neX     using Delta rule(Wildrow-Hoff): e=d
    d = error*outputOfOutputLayer*(1-outputOfOutputLayer)
    partOne = round((learningRate*d*rowOne),5)
    partTwo = round((learningRate*d*rowTwo),5)
    changeIn_Wvector = [partOne,partTwo]    
    new_Wvector = [TWweightVector[0]+partOne,TWweightVector[1]+partTwo]
    # print(f"\nNew Wvector\n{new_Wvector}")

#new Vvector
#calculating quantities e and d*
    eRowOne = TWweightVector[0]*d
    eRowTwo = TWweightVector[1]*d 
    e = [eRowOne,eRowTwo]

    dStarRowOne = round((eRowOne*hiddenLayerOutput[0]*(1-hiddenLayerOutput[0])), 5)
    dStarRowTwo = round((eRowTwo*hiddenLayerOutput[1]*(1-hiddenLayerOutput[1])), 5)
    dStar = [dStarRowOne,dStarRowTwo]

#calculating Xprime {O}i*Transpose(dStar)
    Xprime = [[x1*dStar[0],x1*dStar[1]],[x2*dStar[0],x2*dStar[1]]]

#change in Vvector = nXprime
    changeIn_Vvector = [[learningRate*Xprime[0][0],learningRate*Xprime[0][1]],[learningRate*Xprime[1][0],learningRate*Xprime[1][1]]]
    new_Vvector = [[VweightVector[0][0]+changeIn_Vvector[0][0],VweightVector[0][1]+changeIn_Vvector[0][1]],[VweightVector[1][0]+changeIn_Vvector[1][0],VweightVector[1][1]+changeIn_Vvector[1][1]]]
    # print(f"\nNew Vvector\n{new_Vvector}")#confirm with slide 15 BP NN values(wrong values)
    newWeights =([new_Vvector, new_Wvector])    
    return newWeights


epoch = 1
while(True):
    print("*"*30)
    print("*"*30)
    print(f"\nEPOCH {epoch} ")
    print("*"*30)
    print("*"*30)
    for value in range(len(dataset)):
        print(f"\nBatch {value + 1}")
         
        print(f"Weights before\n\n{weights}\n\n")
        
        weights = batch(dataset[value][0],dataset[value][1],weights[0],weights[1], dataset[value][2])
        weights = list(weights)                      #converting tuple to list
        # print(f"Weights after\n\n{weights}\n\n")
        
        print(f"\nNew Vvector\n: {weights[0]}")
        print(f"\nNew Wvector\n: {weights[1]}")
        
    errorRate = (sum(squaredError))/len(dataset)
    print(f"\nError Rate      : {errorRate}")    
    print(f"\nTolerance Value : {toleranceValue}")
    print(f"\nEnd of epoch {epoch}")
    # time.sleep(0.5)
    # input()                                             #pause here to analyze epoch
    squaredError.clear()
    if(errorRate<toleranceValue):                       #stopping condition
        print(f"\nCONVERGENCE AFTER {epoch} EPOCHS  ")
        break     
    epoch += 1
    
