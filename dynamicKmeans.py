# K Means Clustering

import time, sys
from functools import reduce
# from clusterElectionData import trainingSet as importedElectionData
start_time = time.time()

objects = [(5, 4), (1, 1), (2, 1), (4, 3), (4, 9), (44, 9), (42, 9)]
# objects = importedElectionData

print("\nDATA COLLECTION\nExisting datapoints")
# for x in objects:
#     print(x)
objectSize = len(objects)
print(objectSize)
print(f"Number of datapoints: {objectSize}.")

print("\n*****************\n")

######################
#ALGORITHM STARTS HERE
#######################

def repeatedSection(objects, centroids):
    print(centroids)
    # input()
    newCentroids = []
    print("\nDistance from centroids")
    distances = []
    for centroid in centroids:              #distance of datapoints from each of the centroids
        distance = []
        for item in objects:        
            distanceFromObject = (centroid[0] - item[0])**2 + (centroid[1] - item[1])**2
            
            distance.append(distanceFromObject)
        print(f"\n {centroid} \t  {distance}")
        distances.append(distance)
    
        #print(f"Distances : {distances}")

    minimums = [ pointDistance.index(min(pointDistance))  for pointDistance in zip(*distances) ]

    print(f"\n minimums : {minimums}")


    clusters = [[] for _ in range(len(centroids))]
    
    for i in range(len(objects)):
        clusters[minimums[i]].append(objects[i])
    print("\n The clusters") 

    
    for num, cluster in enumerate(clusters):
        print(f"\n Cluster {num + 1} : {cluster}")
        xCentroidValue = round((reduce(lambda x,y : x + y[0], cluster, 0)/(len(cluster))), 2)
        yCentroidValue = round((reduce(lambda x,y : x + y[1], cluster, 0)/(len(cluster))), 2)
        newCentroids.append((xCentroidValue, yCentroidValue))
        print(f"\n New centroid : {newCentroids[num]}")
        
    print(f"\nNew centroids : {newCentroids}")
    
    return newCentroids
       
while(True):
    k = int(input("\nHow many  clusters do you want?"))

    if(k > 1 and k < objectSize):
        
        centroids = []
        for value in range(k):
            centroids.append(objects[value])

        print(f"\ncentroids (First {k} values)  : {centroids}")
        

        #iteration for stability                         
        
        i = 1
        while(True):
            print(f"\n\n:::::::::::::::::::::ITERATION {i}::::::::::::::::::::::\n")
                     
            # time.sleep(3) 
            
            newCentroids = repeatedSection(objects, centroids)                    
                      
            #print(f"{newCentroids} {centroids}")

            if (newCentroids == centroids):   
                print(f"\n\n*******Clusters stabilized after {i} rounds*****\n\n")
                break
            else:
                centroids = newCentroids.copy()
                i+=1
                continue

        break
        print(compare)


        break
    else:
        print("\nInvalid input. Retry")
        print(f"\nCentroids range 2 to {k-1}")



stop_time =  time.time()

time_taken = abs(round((stop_time - start_time), 3))

print(f"\n Time taken : {time_taken} s")



