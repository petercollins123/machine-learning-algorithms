# Group 6
# K Nearest Neighbor Algorithm
import sys
import csv
import random
import math
import operator

# Responsibe for finding the distance 
# using euclidean and not Manhattan
def calcDistance(dataSet1, dataSet2, attrNumber):
	distance = 0
	for x in range(attrNumber):
		distance += pow((dataSet1[x] - dataSet2[x]), 2)
	return math.sqrt(distance)

# Getting data from file
def readData(filename, pivot, dataSet=[] , testCaseSet=[]):
	with open(filename, 'r') as csvfile:
	    readLines = csv.reader(csvfile)
	    data = list(readLines)
		# Range from 0 to datalength - 1
	    for x in range(len(data)):
			# get the data from 0 to 2 
			# depending on the numeric features
	        for y in range(2):
	            data[x][y] = float(data[x][y])
				# Generates random number between 0 - 1 and if less 
				# less than the pivot 
	        if random.random() < pivot:
	            dataSet.append(data[x])
	        else:
	            testCaseSet.append(data[x])

#Gets Nearest Neighbor function
def getNearestNeighbors(dataSet, testCaseSet, k):
	distances = []
	length = len(testCaseSet)-1
	for x in range(len(dataSet)):
		dist = calcDistance(testCaseSet, dataSet[x], length)
		# Create tuple with the list data with the distance
		distances.append((dataSet[x], dist))
		#print(f"distances_---->: {distances}")
	# Sorts with the distances value appended in Y tuple
	distances.sort(key=operator.itemgetter(1))
	#print(f"distances_sorted---->: {distances}")
	neighbors = []
	for x in range(k):
		#print(f"DISTANCES ***>: {distances[x][0]}")
		#Gets the list from the tuple
		neighbors.append(distances[x][0])
	return neighbors

def getMostCommonAttribute(neighbors):
	labelVotes = {}
	for x in range(len(neighbors)):
		#gets the label
		response = neighbors[x][-1]
		if response in labelVotes:
			labelVotes[response] += 1
		else:
			labelVotes[response] = 1
	sortedVotes = sorted(labelVotes.items(), key=operator.itemgetter(1), reverse=True)
	return sortedVotes[0][0]

#Function that says the accuracy
def measureAccuracy(correctCaseSet, predictions):
	accurateValues = 0
	for x in range(len(correctCaseSet)):
		# Assume the last value in a set is the attribute
		# and if the label equals the preditions
		if correctCaseSet[x][-1] == predictions[x]:
			# then increment
			accurateValues += 1
	# Get accurate over total set size
	percentageAccuracy = accurateValues/float(len(correctCaseSet)) * 100.0
	return percentageAccuracy

# Function validating the K value
def kValidation(dataSet,k):
	dataSize = len(dataSet)
	# 1. K should be greater than 1
	# 2. K should not be equal to data Size
	# 3. K should be odd for voting purposes
	if(k > 1 and k < dataSize and k % 2 != 0):
		return True
	else:
		return False

# Normalization function
def normalization(valList):
	normalizedList = []
	minX = min(valList)
	maxX = max(valList)
	if(maxX > 1): # Do normalization if max is greater than 1
		print ('min and max representaries : ', minX,maxX)
		#Normalized Data
		for item in valList:
			print ('ITEM : ', item)
			normalized = (valList[item]-minX)/(maxX-minX)
			print ('NORM : ', normalized)
			normalizedList.append(normalized)
		print ('NORMALIZED : ', normalizedList)

def execute():
	print(f"-----------------------INSTRUCTIONS------------------------------------------------------")
	print(f"Program Reads from CSV file Tissue Data.\nIt randomizes selection of test case data and treats the rest as training data.")
	print(f"-----------------------------------------------------------------------------------------")

	# Number that will decide if the 
	# random values are less than 0.65 should
	# be the training data and above are the testdata 
	pivot = 0.65
	dataSet = []
	possiblePredictions = []
	testCaseSet = []

	#defa = query_yes_no("Would you like to add your custom data? Format: [1,2,'Bad']")
	#print(f"The answer to bool {defa}")
	# Reads data and separate data to training data and test data[Accuracy]
	readData('paper.data',pivot,dataSet, testCaseSet)
	print(f"TRAINING ITEMS: [{len(dataSet)}] => {dataSet} \nTEST ITEMS: [{len(testCaseSet)}] => {testCaseSet}\n\n")
	# Input value of K
	k = int(input("Enter a K: "))
	# Validate K
	kValid = kValidation(dataSet,k)
	# Valid K
	if(kValid):
	    # Using the testCase
		for testItem in range(len(testCaseSet)):
			neighbours = getNearestNeighbors(dataSet, testCaseSet[testItem],k)
			label = getMostCommonAttribute(neighbours)
			possiblePredictions.append(label)
			
		print(f"The Predictions are: {possiblePredictions}")
		print(f"Actual Data: {testCaseSet}")

		accuracyStatistics = measureAccuracy(testCaseSet, possiblePredictions)
		print(f"ACCURACY PERCENTAGE: {accuracyStatistics}")
	else:
		print(f"ERROR! Enter Valid K: K should be greater than 1, Even Number and not equal to the number of DataSet Items.\n")
# RUN MAIN FUNCTION
execute()