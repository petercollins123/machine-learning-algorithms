#IMPLEMENTATION USING THE ENVIRONMENT IN THE CSC 323 CLASS NOTES WITH NODES(ROOMS) A, B, C, D, E, F WITH F AS THE GOAL STATE
import random, time
#setting learning rate and reward matrix R
learningRate = 0.8
nodes = ['A', 'B', 'C', 'D', 'E', 'F']

print("_"*30)
print("\nMaking the R - Matrix\n")
print("_"*30)



Rmatrix =[
    ["_",	"_",	"_",	"_",	"0",	"_"],
    ["_",	"_",	"_",	"0",	"_",	"100"],
    ["_",	"_",	"_",	"0",	"_",	"_"],
    ["_",	"0",	"0",	"_",	"0",	"_"],
    ["0",	"_",	"_",	"0",	"_",	"100"],
    ["_",	"0",	"_",	"_",	"0",	"100"]
]


def printRmatrix():
    print("*"*20)
    print("\n R-MATRIX\n")
    print("*"*20)
    print(f"\n    {nodes}\n")
    i = 0
    for node in Rmatrix:
        print(f"{nodes[i]} : {node} \n")
        i+=1
printRmatrix()

#initializing Q - MATRIX TO ZEROS
 
    
Qmatrix =[
    [0,	0,	0,	0,	0,	0],
    [0,	0,	0,	0,	0,	0],
    [0,	0,	0,	0,	0,	0],
    [0,	0,	0,	0,	0,	0],
    [0,	0,	0,	0,	0,	0],
    [0,	0,	0,	0,	0,	0]
]

def printQmatrix():
    print("*"*20)
    print("\n Q-MATRIX\n")
    print("*"*20)
    print(f"\n    {nodes}\n")
    i = 0
    for node in Qmatrix:
        print(f"{nodes[i]} : {node} \n")
        i+=1

printQmatrix()

#EPISODES
goalStateReward = 100
goalState = 'F'

path = []       #path followed
QmatrixNormalizationValues = []
initialState = random.choice(nodes)

#function for testing for goal state in each episode
def test(initialState):
    print(f"\nInitial state : {initialState}")
    # time.sleep(2)

    if(initialState == goalState):
        print(f"\nGoal state reached")
        path.append(goalState)
        print(f"\nPath followed : {path}")            
        path.append(initialState)
     
    else: 
        statePosition = nodes.index(initialState)
        rewards = Rmatrix[statePosition]
        print(f"\nRewards[{initialState}] : {rewards}")
        currentStatePossibleActions = []
        positionOfPossibleActions = []
        i = 0
        for action in rewards:
            if(action != "_" and action != ""):
                action = int(action)
                currentStatePossibleActions.append(action)
                positionOfPossibleActions.append(i)
            i+=1    
            
        print(f"\npositions        : {positionOfPossibleActions}")
        print(f"\npossible actions : {currentStatePossibleActions}")
        actionChosen = random.choice(currentStatePossibleActions)
        positionOfChosenAction = currentStatePossibleActions.index(actionChosen)
        positionInNodes = positionOfPossibleActions[positionOfChosenAction]
        print(f"\nAction chosen : {actionChosen} == {nodes[positionInNodes]}")
        
        possibleNextActions = [] 
        for value in range(len(nodes)):    
            possibleNextActions.append(Rmatrix[value][positionInNodes])

        integerPossibleNextActions = []
        positionOfNextPossibleActions = []
        j = 0
        for action in possibleNextActions:
            if(action != "_" and action != ""):
                action = int(action)
                integerPossibleNextActions.append(action)
                positionOfNextPossibleActions.append(j)
            j+=1
        print(f"\nnext possible actions from {nodes[positionInNodes]} : {positionOfNextPossibleActions}")
        print(f"\n {nodes[positionInNodes]} possible actions                : {integerPossibleNextActions}\n")

        correspondingQMatrixValues = []
        for value in positionOfNextPossibleActions:        
            correspondingQMatrixValues.append(Qmatrix[value][positionOfChosenAction])

        print(f"\nCorresponding Q-matrix values : {correspondingQMatrixValues}")
        Qmatrix[statePosition][positionInNodes] = round((actionChosen + (learningRate*(max(correspondingQMatrixValues)))), 2)
        print(f"\nQ[{initialState}, {nodes[positionInNodes]}] = {Qmatrix[statePosition][positionInNodes]}\n ")
    #normalization of Qmatrix values
        QmatrixNormalizationValues.append(Qmatrix[statePosition][positionInNodes])
        # print(f"\nNormalization values : {QmatrixNormalizationValues}")

        testState = Qmatrix[statePosition][positionInNodes]
        print(f"\nNew Qmatrix ")
        printQmatrix()      
    #checking for goal state      
        if(testState == goalStateReward):
            print(f"Q[{initialState}, {nodes[positionInNodes]}] = {Qmatrix[statePosition][positionInNodes]}")
            print(f"\nGoal state reached")
            path.append(goalState)
            print(f"\nPath followed : {path}")
            
    #continue if goal state not found
        else:
            print("\n\n")
            print("*"*20)
            print("*"*20)
            print(f"Current state : {nodes[positionInNodes]}")        
            # time.sleep(2)
            testFor = nodes[positionInNodes]
            path.append(testFor)
            test(testFor)

for i in range(1):
    print("*"*56)
    print(f":::::::::::::::::::::::: EPISODE {i+1} :::::::::::::::::::::")
    print("*"*56)
    # time.sleep(2)
    path.clear()    
    test(initialState)
    print(f"\nEnd of episode {i+1}.\n\n")
    # time.sleep(3)










# Qmatrix  = []
# for node in nodes:
#     pre_Qmatrix = []
#     for node in nodes:
#         pre_Qmatrix.append(0)   



